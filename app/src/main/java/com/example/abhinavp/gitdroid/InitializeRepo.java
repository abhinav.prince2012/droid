package com.example.abhinavp.gitdroid;

import android.os.Environment;
import android.util.Log;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import java.io.File;

/**
 * Created by AbhinavP on 6/10/2016.
 */
public class InitializeRepo {
    private String repoName;
    private Git git;
    public InitializeRepo(String newRepository) {
        repoName = newRepository;
    }

    protected Git call() {
        File folder = new File(Environment.getExternalStorageDirectory() + "/mytestDIRECTORY/" + repoName);
        folder.mkdir();
        Log.e("Success message : ","Folder " + repoName + "created");
        try {
            git = Git.init().setDirectory(folder).call();
            Log.e("Success message : ", repoName  + " initialized inside myTestDIRECTORY");
        } catch (GitAPIException e) {
            e.printStackTrace();
        }
        return git;
    }
}
