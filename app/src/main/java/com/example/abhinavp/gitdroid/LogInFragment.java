package com.example.abhinavp.gitdroid;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import org.eclipse.egit.github.core.Repository;

import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by AbhinavP on 6/16/2016.
 */
public class LogInFragment extends Fragment {

    public View inflated_view;
    public static String USERNAME = "";
    public static String PASSWORD = "";
    public Context context;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        inflated_view = inflater.inflate(R.layout.login_fragment, container, false);

        Log.e("LogInFragment->", "onCreateView()");

        final Button default_login_btn = (Button) inflated_view.findViewById(R.id.defaultloginbtn);
        final Button login_btn = (Button) inflated_view.findViewById(R.id.signinbtn);
        final EditText usernameET = (EditText) inflated_view.findViewById(R.id.usernameet);
        final EditText passwordET = (EditText) inflated_view.findViewById(R.id.passwordet);

        // On click : get username and password and login
        login_btn.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                USERNAME = usernameET.getText().toString();
                PASSWORD = passwordET.getText().toString();
                try {
                    login();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Log.e("lol ", "clicked login");
            }
        }));

        // default login
        default_login_btn.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                USERNAME = "abhinavprince";
                PASSWORD = "lolpass001";
                try {
                    login();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Log.e("lol ", "clicked  default login");
            }
        }));
        return inflated_view;
    }


    // Inflate MyReposotoriesFragment view, start background processes,
    private void login() throws ExecutionException, InterruptedException {

        // Start background processes : login, get list of repositories
        Log.e("LogInFragment->", "login(): Starting Background Processes");
        BackGroundProcesses background_processes = new BackGroundProcesses(context, USERNAME, PASSWORD);
        background_processes.execute().get();
        Log.e("LogInFragment->", "login(): Background Processes executed.");


        // Inflate MyRepositoriesFragment
        Log.e("LogInFragment->", "login(): Inflating MyRepositoriesFragment.");
        MyRepositoriesFragment myrepofragment = new MyRepositoriesFragment();
        myrepofragment.setRepositories(background_processes.repositories);
        myrepofragment.setContext(context);
        inflate_fragment(myrepofragment);
        Log.e("LogInFragment->", "login(): MyRepositoriesFragment inflated.");
        Log.e("LIF->login: size(repo)=", String.valueOf(background_processes.repositories.size()));
    }

    // Replace whatever is in the "fragment_layout" view with "fragment" view
    private void inflate_fragment(Object fragment) {
        Log.e("LogInFragment->", "->infalte_fragment(): enters");
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_layout, (Fragment) fragment);
        transaction.commit();
        Log.e("LogInFragment->", "->infalte_fragment(): exits");
    }
}
