package com.example.abhinavp.gitdroid;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.eclipse.egit.github.core.Gist;
import org.eclipse.egit.github.core.GistFile;
import org.eclipse.egit.github.core.Repository;
import org.eclipse.egit.github.core.client.GitHubClient;
import org.eclipse.egit.github.core.service.GistService;
import org.eclipse.egit.github.core.service.RepositoryService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by AbhinavP on 6/16/2016.
 */
public class BackGroundProcesses extends AsyncTask<Void, Void, Void>{

    private Context context;
    private String username, password;
    public List<Repository> repositories = new ArrayList<Repository>();
    public GitHubClient client = new GitHubClient();

    public BackGroundProcesses(Context context, String USERNAME, String PASSWORD) {
        this.context = context;
        this.username = USERNAME;
        this.password = PASSWORD;
    }

    @Override
    protected Void doInBackground(Void... params) {
        Log.e("BackGroundProcesses->", "doInBackground()");

        client.setCredentials(username, password);

        getListOfRepositories();

        createGist("Hello.java", "Prints a string to standard out", "System.out.println(\"Hello World\");");









/*        OAuthService oauthService = new OAuthService();
        oauthService.getClient().setCredentials("abhinavprince", "password");
        Authorization auth = new Authorization();
        auth.setScopes(Arrays.asList("gist"));
        try {
            auth = oauthService.createAuthorization(auth);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.e("lol : ", auth.getToken() + "\n LOL \n" + auth.toString());*/

        return null;
    }

    private Gist createGist(String fileName, String description, String content) {
        GistFile file = new GistFile();
        file.setContent(content);
        Gist gist = new Gist();
        gist.setDescription(description);
        gist.setFiles(Collections.singletonMap(fileName, file));
        GistService service = new GistService(client);
        try   {
            Log.e("BackGroundProcesses", "->doInBackground->createGist" );
            return gist = service.createGist(gist);
        }
        catch (IOException e) { e.printStackTrace(); }
        return gist;
    }

    private void getListOfRepositories() {
        RepositoryService repositoryService = new RepositoryService(client);
        try {
            for( Repository repo : repositoryService.getRepositories("abhinavprince"))  repositories.add(repo);
        }
        catch (IOException e) {  e.printStackTrace(); }
        Log.e("doInBackground->", "getListOfRepositories()");
    }

}
