package com.example.abhinavp.gitdroid;

import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import java.io.File;

/**
 * Created by AbhinavP on 6/10/2016.
 */
public class CloneRepo extends AsyncTask<Void, Void, Void>{
    private String remoteUrl;
    private Git git;
    public CloneRepo(String REMOTEURL) {
        remoteUrl = REMOTEURL;
    }

    @Override
    protected Void doInBackground(Void... params) {
        String repoLocalPath = Environment.getExternalStorageDirectory() + "/mytestDIRECTORY" + "/" + remoteUrl.split("/")[4].split("\\.")[0];
        File folder = new File(repoLocalPath);
        Log.e("Success message :","Cloning from " + remoteUrl + " to " + folder.toString());
        try {
            git = Git.cloneRepository().setURI(remoteUrl).setDirectory(folder).call();
            Log.e("Success message : ", "Cloning Done");
        } catch (GitAPIException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected Git getGit(){
        return git;
    }
}
