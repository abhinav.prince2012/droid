package com.example.abhinavp.gitdroid;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import org.eclipse.egit.github.core.Repository;

import java.util.List;

/**
 * Created by AbhinavP on 6/16/2016.
 */
public class MyRepositoriesFragment extends Fragment {

    public View inflated_view;
    public Context context;
    public List<Repository> repositories;

    public void setRepositories(List<Repository> repositories) {
        this.repositories = repositories;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        inflated_view = inflater.inflate(R.layout.my_repositories_fragment, container, false);

        Log.e("MyRepositoriesFragment-", "onCreateView");
        Log.e("Size of repositories :", String.valueOf(repositories.size()));

        ScrollView scrollView = (ScrollView) inflated_view.findViewById(R.id.repositories_scrollview);
        LinearLayout child_layout = new LinearLayout(getActivity());
        child_layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        child_layout.setOrientation(LinearLayout.VERTICAL);
        for( Repository repository : this.repositories){
            TextView tv2 = new TextView(getActivity());
            tv2.setPadding(5, 10, 5 , 5 );
            tv2.setText("Name of repo: " + repository.getName()+ " CloneURL: " + repository.getCloneUrl() + " Description : " + repository.getDescription() + " Size: " + repository.getSize() );
            child_layout.addView(tv2);
        }
        scrollView.addView(child_layout);
        return inflated_view;
    }
}
