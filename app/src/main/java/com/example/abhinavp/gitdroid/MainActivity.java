package com.example.abhinavp.gitdroid;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;


public class MainActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Inflate Login Fragment
        LogInFragment loginfragment = new LogInFragment();
        inflate_fragment(loginfragment);
        Log.e("MainActivity->onCreate:", "Username is " + loginfragment.USERNAME);
    }


    // Replace whatever is in the "fragment_layout" view with "fragment" view
    private void inflate_fragment(Object fragment) {
        Log.e("MainActivity->", "->infalte_fragment(): enters");
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_layout, (Fragment) fragment);
        transaction.commit();
        Log.e("MainActivity->", "->infalte_fragment(): exits");
    }

}
